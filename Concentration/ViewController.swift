//
//  ViewController.swift
//  Concentration
//
//  Created by PRO13 on 17.09.2020.
//  Copyright © 2020 Chelebon Studio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    lazy var game: Concentration = Concentration(numberOffPairsOffCards: numberOffPairsOffCards)
    
    var numberOffPairsOffCards: Int {
        return (cardButtons.count + 1) / 2
    }
    
    private(set) var flipCount = 0 { didSet { flipCountLabel.text = "Flips: \(flipCount)" } }
    
    @IBOutlet private weak var flipCountLabel: UILabel!
    
    @IBOutlet private var cardButtons: [UIButton]!
    
    @IBAction private func touchCard(_ sender: UIButton) {
        flipCount += 1
        if let cardNumber = cardButtons.firstIndex(of: sender) {
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("chosen cards")
        }
    }
    
    private func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 0.9450980392, green: 0.3960784314, blue: 0.1333333333, alpha: 0) : #colorLiteral(red: 0.9450980392, green: 0.3960784314, blue: 0.1333333333, alpha: 1)
            }
        }
    }

    private var emojiChoises = ["🤮","🤢","😡","🤑","😷","🤧","🤒","😵","🥶"]
    private var emoji = [Card:String]()
    private func emoji(for card: Card) -> String {
        if emoji[card] == nil, emojiChoises.count > 0 {
            emoji[card] = emojiChoises.remove(at: emojiChoises.count.arc4random)
        }
        return emoji[card] ?? ""
    }
}

extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return -Int(arc4random_uniform(UInt32(self)))
        } else {
            return 0
        }
    }
}
