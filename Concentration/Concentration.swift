//
//  Concentration.swift
//  Concentration
//
//  Created by PRO13 on 30.09.2020.
//  Copyright © 2020 Chelebon Studio. All rights reserved.
//

import Foundation

struct Concentration {
    
    private(set) var cards = [Card]()
    
    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            var foundIndex: Int?
            for index in cards.indices {
                if cards[index].isFaceUp {
                    if foundIndex == nil {
                        foundIndex = index
                    } else {
                        return nil
                    }
                }
            }
            return foundIndex
        }
        set {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
     
       
       mutating func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "Concentration.chooseCart(at: \(index)): chosen index not in the cards")
           if !cards[index].isMatched {
               if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                   if cards[matchIndex] == cards[index] {
                       cards[matchIndex].isMatched = true
                       cards[index].isMatched = true
                   }
                   cards[index].isFaceUp = true
               } else {
                   indexOfOneAndOnlyFaceUpCard = index
               }
           }
       }
    init(numberOffPairsOffCards: Int) {
        assert(numberOffPairsOffCards > 0, "Concentration.init(\(numberOffPairsOffCards)): you must have at least of cards")
        for _ in 1...numberOffPairsOffCards {
        let card = Card()
        cards += [card, card]
        }
        // TODO:  Shuffle the cards
    }
            
            
    
    
}
